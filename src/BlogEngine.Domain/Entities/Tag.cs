﻿using System.Collections.Generic;
namespace BlogEngine.Domain.Entities
{
    public class Tag : BaseEntity
    {
        public Tag()
        {
            PostTags = new HashSet<PostTag>();
        }

        public string Title { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}
