﻿using System.Collections.Generic;

namespace BlogEngine.Domain.Entities
{
    public class Category : BaseEntity
    {
        public Category()
        {
            Posts = new HashSet<Post>();
        }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Post> Posts { get; private set; }
    }
}
