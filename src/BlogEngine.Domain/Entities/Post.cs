﻿using System;
using System.Collections.Generic;

namespace BlogEngine.Domain.Entities
{
    public class Post : BaseEntity
    {
        public Post()
        {
            PostTags = new HashSet<PostTag>();
        }

        public int CategoryId { get; set; }

        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public bool Published { get; set; }

        public DateTime AddedDate { get; set; }

        public string AddedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}
