﻿namespace BlogEngine.Domain.Enumerations
{
    public enum UserType
    {
        Administrator = 1,
        User
    }
}
