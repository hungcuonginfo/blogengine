﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogEngine.Application.Categories.Commands.CreateCategory;
using BlogEngine.Application.Categories.Commands.DeleteCategory;
using BlogEngine.Application.Categories.Commands.UpdateCategory;
using BlogEngine.Application.Categories.Queries.GetCategoryDetail;
using BlogEngine.Application.Categories.Queries.GetCategoryList;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BlogEngine.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : BaseController
    {
        public CategoryController()
        {

        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<CategoryListViewModel>> Get()
        {
            return Ok(await Mediator.Send(new GetCategoryListQuery()));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDetailModel>> Get(int id)
        {
            return Ok(await Mediator.Send(new GetCategoryDetailQuery { Id = id }));
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Create([FromBody] CreateCategoryCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UpdateCategoryCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody] DeleteCategoryCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
