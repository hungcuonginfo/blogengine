﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using BlogEngine.Application.Categories.Queries.GetCategoryDetail;
using BlogEngine.Application.Infrastructure;
using BlogEngine.Application.Infrastructure.AutoMapper;
using BlogEngine.Application.Interfaces;
using BlogEngine.Common;
using BlogEngine.Infrastructure;
using BlogEngine.Persistence;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace BlogEngine.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add AutoMapper
            services.AddAutoMapper(new Assembly[] { typeof(AutoMapperProfile).GetTypeInfo().Assembly });

            // Add framework services.
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IDateTime, MachineDateTime>();

            // Add MediatR
            //services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(GetCategoryDetailHandler).GetTypeInfo().Assembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            // Add DbContext using SQL Server Provider
            services.AddDbContext<IBlogEngineDbContext, BlogEngineDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("BlogEngineDatabase")));

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Info
                {
                    Title = "Blog Engine Api",
                    Version = "v1",
                    Description = "The Blog Engine Service HTTP API",
                    TermsOfService = "Terms Of Service"
                });

                //options.OperationFilter<AuthorizeCheckOperationFilter>();
                //options.OperationFilter<FileUploadOperation>();
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                app.UsePathBase(pathBase);
            }
            app.UseSwagger(c =>
                {
                    c.PreSerializeFilters.Add((swaggerDoc, httpRequest) =>
                    {
                        swaggerDoc.BasePath = !string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty;
                    });
                })
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"{(!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty)}/swagger/v1/swagger.json", "PDFMerger.API V1");
                    c.OAuthClientId("blogengine");
                    c.OAuthAppName("BlogEngine");
                });

            app.UseMvc();
        }
    }
}
