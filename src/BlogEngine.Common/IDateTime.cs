﻿using System;

namespace BlogEngine.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
