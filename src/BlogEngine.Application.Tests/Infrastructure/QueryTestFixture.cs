using System;
using BlogEngine.Persistence;
using Xunit;

namespace BlogEngine.Application.Tests.Infrastructure
{
    public class QueryTestFixture : IDisposable
    {
        public BlogEngineDbContext Context { get; private set; }

        public QueryTestFixture()
        {
            Context = BlogEngineContextFactory.Create();
        }

        public void Dispose()
        {
            BlogEngineContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}