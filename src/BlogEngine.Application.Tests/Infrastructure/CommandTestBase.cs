using System;
using BlogEngine.Persistence;

namespace BlogEngine.Application.Tests.Infrastructure
{
    public class CommandTestBase : IDisposable
    {
        protected readonly BlogEngineDbContext _context;

        public CommandTestBase()
        {
            _context = BlogEngineContextFactory.Create();
        }

        public void Dispose()
        {
            BlogEngineContextFactory.Destroy(_context);
        }
    }
}