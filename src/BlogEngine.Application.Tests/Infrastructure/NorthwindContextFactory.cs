using System;
using Microsoft.EntityFrameworkCore;
using BlogEngine.Domain.Entities;
using BlogEngine.Persistence;

namespace BlogEngine.Application.Tests.Infrastructure
{
    public class BlogEngineContextFactory
    {
        public static BlogEngineDbContext Create()
        {
            var options = new DbContextOptionsBuilder<BlogEngineDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new BlogEngineDbContext(options);

            context.Database.EnsureCreated();

            //context.Customers.AddRange(new[] {
            //    new Customer { CustomerId = "ADAM", ContactName = "Adam Cogan" },
            //    new Customer { CustomerId = "JASON", ContactName = "Jason Taylor" },
            //    new Customer { CustomerId = "BREND", ContactName = "Brendan Richards" },
            //});

            context.SaveChanges();

            return context;
        }

        public static void Destroy(BlogEngineDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}