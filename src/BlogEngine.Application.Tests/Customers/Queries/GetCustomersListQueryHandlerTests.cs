﻿using BlogEngine.Application.Customers.Models;
using BlogEngine.Application.Customers.Queries;
using BlogEngine.Application.Tests.Infrastructure;
using BlogEngine.Persistence;
using Shouldly;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BlogEngineTraders.Application.UnitTests.Infrastructure
{
    [Collection("QueryCollection")]
    public class GetCustomersListQueryHandlerTests
    {
        private readonly BlogEngineDbContext _context;

        public GetCustomersListQueryHandlerTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetCustomersTest()
        {
            var sut = new GetCustomersListQueryHandler(_context);

            var result = await sut.Handle(new GetCustomerListQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<CustomerListModel>>();

            result.Count.ShouldBe(3);
        }
    }
}