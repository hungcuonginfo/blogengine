﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlogEngine.Domain.Entities;

namespace BlogEngine.Persistence
{
    public class BlogEngineInitializer
    {

        public static void Initialize(BlogEngineDbContext context)
        {
            var initializer = new BlogEngineInitializer();
            initializer.SeedEverything(context);
        }

        public void SeedEverything(BlogEngineDbContext context)
        {
            context.Database.EnsureCreated();
            // Look for category if existing then return
            if (context.Categories.Any())
            {
                return;
            }

            var categories = new Category[] {
                new Category{ Name=".Net Core" },
                new Category{ Name="EF Core" },
            };

            context.Categories.AddRange(categories);
            context.SaveChanges();


            var posts = new Post[] {
                new Post{ CategoryId=1,Title="Get started with .Net Core",FullDescription="Create new .Net Core app" },
                new Post{ CategoryId=2,Title="Get started with EF Core",FullDescription="Apply Ef core code first" },
            };

            context.Posts.AddRange(posts);
            context.SaveChanges();
        }
    }
}