﻿using BlogEngine.Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using BlogEngine.Domain.Entities;
using BlogEngine.Persistence.Extensions;

namespace BlogEngine.Persistence
{
    public class BlogEngineDbContext : DbContext, IBlogEngineDbContext
    {
        public BlogEngineDbContext(DbContextOptions<BlogEngineDbContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<PostTag> PostTags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyAllConfigurations();
        }
    }
}
