﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BlogEngine.Domain.Entities;

namespace BlogEngine.Persistence.Configurations
{
    public class PostTagConfiguration : IEntityTypeConfiguration<PostTag>
    {
        public void Configure(EntityTypeBuilder<PostTag> builder)
        {
            builder.HasKey(o=> new { o.PostId, o.TagId});
            builder
                .HasOne(o => o.Post)
                .WithMany(o => o.PostTags)
                .HasForeignKey(o=>o.PostId);
            builder
                .HasOne(o => o.Tag)
                .WithMany(o => o.PostTags)
                .HasForeignKey(o => o.TagId);
        }
    }
}
