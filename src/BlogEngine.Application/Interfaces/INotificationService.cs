﻿using BlogEngine.Application.Notifications.Models;

namespace BlogEngine.Application.Interfaces
{
    public interface INotificationService
    {
        void Send(Message message);
    }
}
