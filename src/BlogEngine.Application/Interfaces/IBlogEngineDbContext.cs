﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BlogEngine.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Application.Interfaces
{
    public interface IBlogEngineDbContext
    {
        DbSet<Category> Categories { get; set; }

        DbSet<Post> Posts { get; set; }

        DbSet<PostTag> PostTags { get; set; }

        DbSet<Tag> Tags { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
