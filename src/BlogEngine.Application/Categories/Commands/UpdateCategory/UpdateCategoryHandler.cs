﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BlogEngine.Application.Exceptions;
using BlogEngine.Application.Interfaces;
using BlogEngine.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Application.Categories.Commands.UpdateCategory
{
    public class UpdateCategoryHandler : IRequestHandler<UpdateCategoryCommand>
    {
        private readonly IBlogEngineDbContext _context;

        public UpdateCategoryHandler(
            IBlogEngineDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var entity = await  _context.Categories.SingleOrDefaultAsync(c=>c.Id==request.Id,cancellationToken);
            if (entity == null)
            {
                throw  new  NotFoundException(nameof(Category),request.Id);
            }

            entity.Name = request.Name;
            entity.Description = request.Description;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
