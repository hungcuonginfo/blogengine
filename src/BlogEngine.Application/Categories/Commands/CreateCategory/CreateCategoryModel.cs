﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;

namespace BlogEngine.Application.Categories.Commands.CreateCategory
{
    public class CreateCategoryModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
