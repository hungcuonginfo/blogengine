﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MediatR;

namespace BlogEngine.Application.Categories.Commands.CreateCategory
{
    public class CreateCategoryCommand : IRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
