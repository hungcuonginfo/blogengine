﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BlogEngine.Application.Exceptions;
using BlogEngine.Application.Interfaces;
using BlogEngine.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore.Internal;

namespace BlogEngine.Application.Categories.Commands.DeleteCategory
{
    public class DeleteCategoryHandler : IRequestHandler<DeleteCategoryCommand>
    {
        private readonly IBlogEngineDbContext _context;

        public DeleteCategoryHandler(
            IBlogEngineDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Categories.FindAsync(request.Id);

            if (entity == null)
            {
                throw new  NotFoundException(nameof(Category),request.Id);
            }

            var hasPosts = _context.Posts.Any(o=>o.CategoryId==request.Id);
            if (hasPosts)
            {
                throw new DeleteFailureException(nameof(Category),request.Id,"There are existing posts associated with this category");
            }

            _context.Categories.Remove(entity);
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
