﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace BlogEngine.Application.Categories.Commands.DeleteCategory
{
    public class DeleteCategoryCommand :  IRequest
    {
        public int Id { get; set; }
    }
}
