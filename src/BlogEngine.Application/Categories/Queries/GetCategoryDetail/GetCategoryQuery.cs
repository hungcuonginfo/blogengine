﻿using MediatR;

namespace BlogEngine.Application.Categories.Queries.GetCategoryDetail
{
    public class GetCategoryDetailQuery : IRequest<CategoryDetailModel>
    {
        public int Id { get; set; }
    }
}
