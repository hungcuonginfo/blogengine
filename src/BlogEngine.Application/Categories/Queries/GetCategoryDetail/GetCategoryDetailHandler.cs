﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BlogEngine.Application.Exceptions;
using BlogEngine.Application.Interfaces;
using BlogEngine.Domain.Entities;
using MediatR;

namespace BlogEngine.Application.Categories.Queries.GetCategoryDetail
{
    public class GetCategoryDetailHandler : IRequestHandler<GetCategoryDetailQuery, CategoryDetailModel>
    {
        private  readonly IBlogEngineDbContext _context;

        public GetCategoryDetailHandler(IBlogEngineDbContext context)
        {
            _context = context;
        }

        public async Task<CategoryDetailModel> Handle(GetCategoryDetailQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Categories.FindAsync(request.Id);
            if (entity == null)
            {
                throw new NotFoundException(nameof(Category),request.Id);
            }
            return CategoryDetailModel.Create(entity);
        }
    }
}
