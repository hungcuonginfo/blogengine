﻿using System;
using System.Linq.Expressions;
using BlogEngine.Domain.Entities;

namespace BlogEngine.Application.Categories.Queries.GetCategoryDetail
{
    public class CategoryDetailModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public static Expression<Func<Category, CategoryDetailModel>> Projection
        {
            get
            {
                return  category=> new CategoryDetailModel
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description
                };
            }
        }

        public static CategoryDetailModel Create(Category category)
        {
            return Projection.Compile().Invoke(category);
        }
    }
}
