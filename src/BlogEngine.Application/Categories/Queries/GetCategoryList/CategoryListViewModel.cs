﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogEngine.Application.Categories.Queries.GetCategoryList
{
    public class CategoryListViewModel
    {
        public IList<GetCategoryListLookupModel> Categories { get; set; }
    }
}
