﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogEngine.Application.Interfaces.Mapping;
using BlogEngine.Domain.Entities;

namespace BlogEngine.Application.Categories.Queries.GetCategoryList
{
    public class GetCategoryListLookupModel : IHaveCustomMapping
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Category, GetCategoryListLookupModel>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                .ForMember(x=>x.Name,y=>y.MapFrom(z=>z.Name));
        }
    }
}
