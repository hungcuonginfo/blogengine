﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogEngine.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Application.Categories.Queries.GetCategoryList
{
    public class GetCategoryListHandler : IRequestHandler<GetCategoryListQuery,CategoryListViewModel>
    {
        private  readonly IBlogEngineDbContext _context;
        private readonly IMapper _mapper;
        public GetCategoryListHandler(
            IBlogEngineDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CategoryListViewModel> Handle(GetCategoryListQuery request, CancellationToken cancellationToken)
        {
            var categories = await _context.Categories
                .ProjectTo<GetCategoryListLookupModel>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken);

            return new CategoryListViewModel
            {
                Categories = categories
            };
        }
    }
}
